# Datamining Project | Image Classification

## Overview
An image classification website to classify cat's eye color using an AI model trained using CNN.

## Contributor
- Muhammad Arief Rahman
- Salshadilla Aldakas Erdi
